'use strict';
// Start a PouchDB with Users
var userDB = new PouchDB("members"),
	carDB = new PouchDB("car"),
	deviceDB = new PouchDB("devices"),
	phoneDB = new PouchDB("phone"),
	milesDB = new PouchDB("miles"),
	velocityDB = new PouchDB("velocity_conf"),
	rotationDB = new PouchDB("rotation_conf"),
	specialDB = new PouchDB("spec_conf"),
	canDB = new PouchDB("can_conf"),
	alarmIODB = new PouchDB("alarmIO_conf"),
	timeDB = new PouchDB("time_conf"),
	spdLmtAlarmDB = new PouchDB("spdlimit_alarm_conf"),
	advSpdLmtAlarmDB = new PouchDB("advanced_spdlimit_alarm_conf"),
	gSensorDB = new PouchDB("gsensor_conf"),
	netDB = new PouchDB("net_conf"),
	usbAuthDB = new PouchDB("usb_auth");


var	users = [
		{
			_id: "user",
			password: "12345",
			auth: "user",
		}, 
		{
			_id: "admin",
			password: "54313",
			auth: "admin",
		}, 
		{
			_id: "root",
			password: "3353",
			auth: "root",
		}
	];

// Initialize the membership PouchDB
userDB.init = function (){
	for (var i in users) {
		this.put(users[i]).then(function (response) {
			console.log(response);
		}).catch(function (err) {
			console.log(err);
		});
	}	
}

//
window.onload = userDB.init();

